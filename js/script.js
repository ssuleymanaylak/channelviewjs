var channelName = 'LeFloofTV';
var vidWidth = 500;
var vidHeight = 400;
var vidResults = 10; //maximum number to show videos in page 
var favoriteArray = [];
var arrayUpload =[];//array for Uploads
var nextPageToken = '';

$(document).ready(function(){
	$.get(
		"https://www.googleapis.com/youtube/v3/channels",{
			part: 'contentDetails',
			forUsername: channelName,
			key:'AIzaSyAapY9fRBDQMWtCUpATN9EzQJvClraitcg'},
			//retrieveMyUploads()
			function(data){
				$.each(data.items,function(i,item){
					console.log(item);
					cid= item.id;
					pid = item.contentDetails.relatedPlaylists.uploads;
					fid = item.contentDetails.relatedPlaylists.favorites;
					lid = item.contentDetails.relatedPlaylists.likes;
					getCids(cid);
					getVids(pid);
					getFids(fid);
					getLids(lid);

				})
			}
	);
	//Users' Statistical Infos
	function getCids(cid){
		$.get(
		"https://www.googleapis.com/youtube/v3/channels",{
			part: 'statistics',
			//maxResults: vidResults,
			id: cid,
			key:'AIzaSyAapY9fRBDQMWtCUpATN9EzQJvClraitcg'},
			function(data){
				var output;
				$.each(data.items,function(i,item){
					console.log(item);
					viewCount = item.statistics.viewCount;
					commentCount = item.statistics.commentCount;
					subscriberCount = item.statistics.subscriberCount;
					videoCount = item.statistics.videoCount;
					
					
				
					//Append to results 
					$("#results").append('<p> Channel Name:' + channelName + '</p>');
					$("#results").append('<p> View Count:' + viewCount + '</p>');
					$("#results").append('<p> Comment Count:' + commentCount + '</p>');
					$("#results").append('<p> Subscriber Count:' + subscriberCount + '</p>');
					$("#results").append('<p> Video Count:' + videoCount + '</p>');
				})
			}
	);
	}
	//Users' Uploads

	function getVids(pid){
		$.get(
        "https://www.googleapis.com/youtube/v3/playlistItems",{
        part : 'snippet', 
		maxResults : 50,
		pageToken: nextPageToken,
        playlistId : pid,
        key: 'AIzaSyAapY9fRBDQMWtCUpATN9EzQJvClraitcg'},
        function(data) {
            var results;
			

            $.each( data.items, function( i, item ) {
                results = '<li>'+ item.snippet.title +'</li>';
                $('#results').append(results);
				videoId = item.snippet.resourceId.videoId;
				arrayUpload.push("http://www.youtube.com/watch?v="+videoId);//storing video links in a array
				//$('#results').append("http://www.youtube.com/watch?v="+videoId);
				
            });
			$('#results').append(arrayUpload.join(": "));
        }
			
			);
		
	}
	//alert(arrayUpload.join());
	//document.writeln(arrayUpload);
	//Users' Favorites
	function getFids(fid){
		$.get(
		"https://www.googleapis.com/youtube/v3/playlistItems",{
			part: 'snippet',
			maxResults: vidResults,
			playlistId: fid,
			key:'AIzaSyAapY9fRBDQMWtCUpATN9EzQJvClraitcg'},
			function(data){
				var output;
				$.each(data.items,function(i,item){
					console.log(item);
					videoTitle = item.snippet.title;
					videoId = item.snippet.resourceId.videoId;
					
					//favoriteArray.push('<li> src=\"//www.youtube.com/embed/'+videoId+'\</li>');
					//Append to results listStyleType
		
					//$('#results').append(favoriteArray);
				})
			}
			
	);
	}

	//Users' Likes
	function getLids(lid){
		$.get(
		"https://www.googleapis.com/youtube/v3/playlistItems",{
			part: 'snippet',
			maxResults: vidResults,
			playlistId: lid,
			key:'AIzaSyAapY9fRBDQMWtCUpATN9EzQJvClraitcg'},
			function(data){
				var output;
				$.each(data.items,function(i,item){
					console.log(item);
					videoTitle = item.snippet.title;
					videoId = item.snippet.resourceId.videoId;
					
					output = '<li><iframe height="'+vidHeight+'" width="'+vidWidth+'" src=\"//www.youtube.com/embed/'+videoId+'\"></iframe></li>';
					//Append to results listStyleType
					//$('#results').append(output);
				})
			}
	);
	}
});
		

	
